local _, Addon = ...

SLASH_ItemDB1 = "/itemdb";

SlashCmdList['ItemDB'] = function(msg)
	local args = {}
	for substring in msg:gmatch("%S+") do
	   table.insert(args, substring)
	end
	if msg == "clear" then 
		Addon.Ludwig_Items = {};
		Addon.Items = {};
		Addon.LastItemID = 0;
		print("Items cleared");
	elseif msg == "export" then
		Addon:Export();
	elseif tonumber(args[1]) ~= nil and tonumber(args[2]) ~= nil then
		Addon:ScanRange(tonumber(args[1]), tonumber(args[2]));
	elseif tonumber(args[1]) ~= nil then
		Addon:Scan(tonumber(args[1]));
	else
		print("Item Database")
		print("/invxml <max> - Scans item id's up to max");
	end
end