local _, Addon = ...

local itemInvTypes = {
	["INVTYPE_NON_EQUIP"] = 0,
	["INVTYPE_HEAD"] = 1,
	["INVTYPE_NECK"] = 2,
	["INVTYPE_SHOULDER"] = 3,
	["INVTYPE_BODY"] = 4,
	["INVTYPE_CHEST"] = 5,
	["INVTYPE_WAIST"] = 6,
	["INVTYPE_LEGS"] = 7,
	["INVTYPE_FEET"] = 8,
	["INVTYPE_WRIST"] = 9,
	["INVTYPE_HAND"] = 10,
	["INVTYPE_FINGER"] = 11,
	["INVTYPE_TRINKET"] = 12,
	["INVTYPE_WEAPON"] = 13,
	["INVTYPE_SHIELD"] = 14,
	["INVTYPE_RANGED"] = 15,
	["INVTYPE_CLOAK"] = 16,
	["INVTYPE_2HWEAPON"] = 17,
	["INVTYPE_BAG"] = 18,
	["INVTYPE_TABARD"] = 19,
	["INVTYPE_ROBE"] = 20,
	["INVTYPE_WEAPONMAINHAND"] = 21,
	["INVTYPE_WEAPONOFFHAND"] = 22,
	["INVTYPE_HOLDABLE"] = 23,
	["INVTYPE_AMMO"] = 24,
	["INVTYPE_THROWN"] = 25,
	["INVTYPE_RANGEDRIGHT"] = 26,
	["INVTYPE_QUIVER"] = 27,
	["INVTYPE_RELIC"] = 28
}

local lastItemID = 0;

function Addon:Scan(itemID)
	local itemName, _, itemRarity, _, itemMinLevel, _, _, _, itemEquipLoc, _, _, itemClassID, itemSubClassID = GetItemInfo(itemID);
	if itemID == 19019 then
		print("19019")
		print(itemName)
	end
	if itemName ~= nil then
		if Addon.Items[itemID] == nil then
			local itemInvType = itemInvTypes[itemEquipLoc];
			if itemInvType == nil then itemInvType = 0 end;
			local itemID36 = Addon:CovertToBase36(itemID);
			local itemString = itemID36 .. itemName;
			Addon.Items[itemID] = {itemClassID, itemSubClassID, itemInvType, itemRarity, itemMinLevel, itemString};
			return true;
		end
	end
	
	return false;
end

function Addon:ScanRange(from, to)
	print("Scanning from " .. from .. " to " .. to);
	
	local itemcount = 0;
	
	for itemID=from,to do
		local status = Addon:Scan(itemID);
		if status then itemcount = itemcount + 1 end;
		lastItemID = itemID;
	end
	
	print("Found " .. itemcount .. " new items");
end

function Addon:Export()
	print("Export init");
	
	Addon.Ludwig_Items = {};

	for itemID, itemInfo in pairs(Addon.Items) do
		local itemClassID = itemInfo[1];
		local itemSubClassID = itemInfo[2];
		local itemInvType = itemInfo[3];
		local itemRarity = itemInfo[4];
		local itemMinLevel = itemInfo[5];
		local itemString = itemInfo[6];
			
		if Addon.Ludwig_Items[itemClassID] == nil then Addon.Ludwig_Items[itemClassID] = {} end
		if Addon.Ludwig_Items[itemClassID][itemSubClassID] == nil then Addon.Ludwig_Items[itemClassID][itemSubClassID] = {} end
		if Addon.Ludwig_Items[itemClassID][itemSubClassID][itemInvType] == nil then Addon.Ludwig_Items[itemClassID][itemSubClassID][itemInvType] = {} end
		if Addon.Ludwig_Items[itemClassID][itemSubClassID][itemInvType][itemRarity] == nil then Addon.Ludwig_Items[itemClassID][itemSubClassID][itemInvType][itemRarity] = {} end
		
		local prevValue = Addon.Ludwig_Items[itemClassID][itemSubClassID][itemInvType][itemRarity][itemMinLevel];
		
		if prevValue == nil then
			Addon.Ludwig_Items[itemClassID][itemSubClassID][itemInvType][itemRarity][itemMinLevel] = itemString;
		else
			Addon.Ludwig_Items[itemClassID][itemSubClassID][itemInvType][itemRarity][itemMinLevel] = prevValue .. "_" .. itemString;
		end
	end
	
	print("Export end");
end

function Addon:CovertToBase36(number)
	-- print("started covert");
	n = floor(number);
    local digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    local t = {};
	for i=4,0,-1 do
		local d = (n % 36) + 1;
		-- print("d: " .. d);
        n = floor(n / 36);
		-- print("n: " .. n);
		local digit = digits:sub(d,d);
		-- print("digit: " .. digit);
        t[i] = digit;
	end
	local r = table.concat(t,"");
	-- print("ended covert: " .. r);
    return r;
end
