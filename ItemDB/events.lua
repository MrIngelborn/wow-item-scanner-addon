local AddonName, Addon = ...

local frame = CreateFrame("FRAME");
frame:RegisterEvent("ADDON_LOADED");
frame:RegisterEvent("PLAYER_LOGOUT");

function frame:OnEvent(event, arg1)
	if event == "ADDON_LOADED" and arg1 == AddonName then
		-- Load variables
		if Items == nil then Items = {} end
		if LastItemID == nil then LastItemID = 0 end
		if Ludwig_Items == nil then Ludwig_Items = 0 end
		
		Addon.Items = Items;
		Addon.LastItemID = LastItemID;
		Addon.Ludwig_Items = Ludwig_Items;
	elseif event == "PLAYER_LOGOUT" then
		
		-- Save variables
		Items = Addon.Items;
		LastItemID = Addon.LastItemID;
		Ludwig_Items = Addon.Ludwig_Items;
	end
end
frame:SetScript("OnEvent", frame.OnEvent);